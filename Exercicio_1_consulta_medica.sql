/* Exercício de Banco de Dados - Luiz ALberto Binda Venturote - M19 */

CREATE DATABASE exercicio;

USE exercicio;

CREATE TABLE especialidade (
  cod_especialidade VARCHAR(3) NOT NULL,
  descricao VARCHAR(35) NOT NULL,
  PRIMARY KEY (cod_especialidade) 
);

CREATE TABLE medico (
  id INT NOT NULL AUTO_INCREMENT,
  cod_especialidade VARCHAR(3) NOT NULL,
  nome VARCHAR(40) NOT NULL,
  sexo VARCHAR(1) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT cod_especialidade
    FOREIGN KEY (cod_especialidade)
    REFERENCES especialidade(cod_especialidade)
    ON DELETE RESTRICT
);

CREATE  TABLE cliente (
  id INT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(40) NOT NULL,
  sexo VARCHAR(1) NOT NULL,
  nascimento DATE NOT NULL,
  telefone VARCHAR(16) NOT NULL,
  PRIMARY KEY (id) 
);

CREATE  TABLE convenio (
  id_convenio INT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(60) NOT NULL,
  PRIMARY KEY (id_convenio) 
);

CREATE  TABLE agenda (
  data_agendada DATE NOT NULL,
  id_medico INT NOT NULL,
  id_cliente INT NOT NULL,
  id_convenio INT NOT NULL,
  valor_cliente DECIMAL(10,2) NOT NULL,
  valor_convenio DECIMAL(10,2) NOT NULL,
  PRIMARY KEY (data_agendada, id_medico, id_cliente),

  CONSTRAINT id_medico
    FOREIGN KEY (id_medico)
    REFERENCES medico(id)
    ON DELETE RESTRICT,

  CONSTRAINT id_cliente
    FOREIGN KEY (id_cliente)
    REFERENCES cliente(id)
    ON DELETE RESTRICT,

  CONSTRAINT id_convenio
    FOREIGN KEY (id_convenio)
    REFERENCES convenio(id_convenio)
    ON DELETE RESTRICT
);

INSERT INTO cliente (nome, sexo, nascimento, telefone) VALUES ('Luiz Alberto', 'M', '1994-02-21', '37114500');
INSERT INTO cliente (nome, sexo, nascimento, telefone) VALUES ('Matheus Claudino', 'M', '1996-08-15', '37489563');
INSERT INTO cliente (nome, sexo, nascimento, telefone) VALUES ('Eduardo', 'M', '1995-03-25', '99874581');
INSERT INTO cliente (nome, sexo, nascimento, telefone) VALUES ('Naiara', 'F', '1990-09-20', '37485621');
INSERT INTO cliente (nome, sexo, nascimento, telefone) VALUES ('Larissa', 'F', '1998-06-01', '81945628');

INSERT INTO especialidade (cod_especialidade, descricao) VALUES ('PED', 'Pediatra');
INSERT INTO especialidade (cod_especialidade, descricao) VALUES ('CIR', 'Cirurgião');
INSERT INTO especialidade (cod_especialidade, descricao) VALUES ('OTO', 'Otorrino');
INSERT INTO especialidade (cod_especialidade, descricao) VALUES ('ORT', 'Ortopedista');
INSERT INTO especialidade (cod_especialidade, descricao) VALUES ('CLG', 'Clinico Geral');

INSERT INTO medico (cod_especialidade, nome, sexo) VALUES ('PED', 'Maria Clara', 'F');
INSERT INTO medico (cod_especialidade, nome, sexo) VALUES ('CIR', 'Hugo Santos', 'M');
INSERT INTO medico (cod_especialidade, nome, sexo) VALUES ('OTO', 'Adalto Gomes', 'M');
INSERT INTO medico (cod_especialidade, nome, sexo) VALUES ('ORT', 'Joana Bone', 'F');
INSERT INTO medico (cod_especialidade, nome, sexo) VALUES ('CLG', 'Jardel Oliveira', 'M');

INSERT INTO convenio (nome) VALUES ('Blue Life');
INSERT INTO convenio (nome) VALUES ('Saúde Total');
INSERT INTO convenio (nome) VALUES ('Mais Saúde');
INSERT INTO convenio (nome) VALUES ('CarePlus');
INSERT INTO convenio (nome) VALUES ('Ômega Saúde');

INSERT INTO agenda (data_agendada,id_medico,id_cliente,id_convenio,valor_cliente,valor_convenio) VALUES ('2013-08-10','4','2','5','200.00','100.00');
INSERT INTO agenda (data_agendada,id_medico,id_cliente,id_convenio,valor_cliente,valor_convenio) VALUES ('2013-08-18','1','3','2','100.00','100.00');
INSERT INTO agenda (data_agendada,id_medico,id_cliente,id_convenio,valor_cliente,valor_convenio) VALUES ('2013-08-18','3','2','4','30.00','20.00');
INSERT INTO agenda (data_agendada,id_medico,id_cliente,id_convenio,valor_cliente,valor_convenio) VALUES ('2013-08-23','1','2','3','500.00','200.00');
INSERT INTO agenda (data_agendada,id_medico,id_cliente,id_convenio,valor_cliente,valor_convenio) VALUES ('2013-11-01','3','2','4','100.00','200.00');
INSERT INTO agenda (data_agendada,id_medico,id_cliente,id_convenio,valor_cliente,valor_convenio) VALUES ('2013-11-25','1','3','1','200.00','0.00');
INSERT INTO agenda (data_agendada,id_medico,id_cliente,id_convenio,valor_cliente,valor_convenio) VALUES ('2013-11-25','2','1','5','300.00','100.00');
INSERT INTO agenda (data_agendada,id_medico,id_cliente,id_convenio,valor_cliente,valor_convenio) VALUES ('2013-11-25','3','2','4','100.00','0.00');
INSERT INTO agenda (data_agendada,id_medico,id_cliente,id_convenio,valor_cliente,valor_convenio) VALUES ('2013-11-25','4','4','2','80.00','50.00');
INSERT INTO agenda (data_agendada,id_medico,id_cliente,id_convenio,valor_cliente,valor_convenio) VALUES ('2013-11-25','5','5','3','50.00','100.00');
INSERT INTO agenda (data_agendada,id_medico,id_cliente,id_convenio,valor_cliente,valor_convenio) VALUES ('2013-12-10','1','1','1','874.00','654.00');
INSERT INTO agenda (data_agendada,id_medico,id_cliente,id_convenio,valor_cliente,valor_convenio) VALUES ('2013-12-10','3','2','5','14.00','80.00');
INSERT INTO agenda (data_agendada,id_medico,id_cliente,id_convenio,valor_cliente,valor_convenio) VALUES ('2013-12-10','4','2','1','100.00','20.00');
INSERT INTO agenda (data_agendada,id_medico,id_cliente,id_convenio,valor_cliente,valor_convenio) VALUES ('2013-12-10','4','3','2','45.00','47.00');

/* Exibir o nome do convênio, o nome do cliente e a data da consulta para toda consulta agendada entre 10 de junho de 2013 e 30 de novembro de 2013.*/
SELECT convenio.nome, cliente.nome, agenda.data_agendada FROM convenio, cliente, agenda WHERE agenda.id_cliente = cliente.id AND agenda.id_convenio = convenio.id_convenio AND agenda.data_agendada BETWEEN '2013-11-1' AND '2013-11-10';

/* Exibir o nome do convênio e a quantidade de consultas agendadas para a data de 10 de dezembro de 2013.*/
SELECT convenio.nome, agenda.data_agendada, COUNT(*) AS Quantidade_de_Consultas FROM convenio, agenda WHERE agenda.id_convenio = convenio.id_convenio AND agenda.data_agendada BETWEEN '2013-12-10' AND '2013-12-10' GROUP BY convenio.nome;

/* Nome dos médicos e a quantia gerada em dinheiro por todas as consultas atendidas (valor_cliente + valor_convenio) no mês de agosto de 2013. Observe que para cada consulta há a possibilidade de um Plano de Saúde pagar parte do valor e o cliente pagar a outra parcela. */
SELECT medico.nome, SUM(agenda.valor_cliente + agenda.valor_convenio) AS total, agenda.data_agendada FROM medico, agenda WHERE agenda.id_medico = medico.id AND MONTH(agenda.data_agendada) = 8 AND YEAR(agenda.data_agendada) = 2013 GROUP BY agenda.data_agendada, medico.nome;

/* Apresentar o valor das consultas pagas pelo plano e consultas particular. Quando o cliente é particular o valor pago pelo convênio é sempre zero. */
SELECT agenda.data_agendada, agenda.valor_cliente AS valor_consulta_particular FROM agenda WHERE agenda.valor_cliente > 0 AND agenda.valor_convenio <= 0 ;
SELECT agenda.data_agendada, agenda.valor_convenio AS valor_pago_plano FROM agenda WHERE agenda.valor_convenio > 0 ;

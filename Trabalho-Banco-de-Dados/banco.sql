CREATE DATABASE Trabalho_LuizAlberto_MatheusClaudino;
USE Trabalho_LuizAlberto_MatheusClaudino;


/*  =================== CRIAÇÃO DE TABELAS ============================ */
/* CRIANDO A TABELA genero*/
CREATE TABLE genero(
   id CHAR(3) NOT NULL PRIMARY KEY, 
   descricao VARCHAR(25) NOT NULL 
);

/* CRIANDO A TABELA usuario*/
CREATE TABLE usuario(
   id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
   nome VARCHAR(50) NOT NULL,
   sexo CHAR(1) NOT NULL,
   data_nascimento DATE NOT NULL,
   endereco VARCHAR(60) NOT NULL 
);

/* CRIANDO A TABELA obra*/
CREATE TABLE obra(
   id INT NOT NULL AUTO_INCREMENT,
   titulo VARCHAR(60) NOT NULL,
   id_genero CHAR(3) NOT NULL,
   PRIMARY KEY (id),
   FOREIGN KEY (id_genero) REFERENCES
   genero (id) ON DELETE RESTRICT
);

/* CRIANDO A TABELA autor*/
CREATE TABLE autor(
   id INT NOT NULL AUTO_INCREMENT,
   nome VARCHAR(60) NOT NULL,
   PRIMARY KEY(id)
);

/* CRIANDO A TABELA autoria*/
CREATE TABLE autoria(
   id_obra INT NOT NULL,
   id_autor INT NOT NULL,
   
   PRIMARY KEY (id_obra, id_autor),
   
   FOREIGN KEY (id_obra)
   REFERENCES obra (id)
   ON DELETE RESTRICT,
   
   FOREIGN KEY(id_autor)
   REFERENCES autor (id)
   ON DELETE CASCADE
);

/* CRIANDO A TABELA editora*/
CREATE TABLE editora (
   id CHAR(5) NOT NULL,
   nome_fantasia VARCHAR(60) NOT NULL,
   PRIMARY KEY(id) 
);

/* CRIANDO A TABELA livro*/
CREATE TABLE livro (
   id INT NOT NULL AUTO_INCREMENT, 
   data_aquisicao DATE NOT NULL,
   edicao INT,
   id_obra INT NOT NULL,
   id_editora CHAR(5) NOT NULL,

   PRIMARY KEY (id),
   FOREIGN KEY (id_obra) REFERENCES obra (id) ON DELETE RESTRICT,
   FOREIGN KEY (id_editora) REFERENCES editora (id) ON DELETE RESTRICT 
);

/* CRIANDO A TABELA movimentacao*/
CREATE TABLE movimentacao(
   id_usuario INT NOT NULL,
   id_livro INT NOT NULL,
   data_emprestimo DATE NOT NULL,
   data_prevista DATE NOT NULL,
   data_devolucao DATE,
   multa NUMERIC(5,2),
   PRIMARY KEY (id_usuario, id_livro, data_emprestimo),
   FOREIGN KEY (id_usuario) REFERENCES usuario (id) ON DELETE RESTRICT,
   FOREIGN KEY (id_livro) REFERENCES  livro (id) ON DELETE RESTRICT 
);



/*  =================== INSERSÃO DE DADOS ============================ */
/* INSERINDO DADOS NA TABELA genero */
INSERT INTO genero (id, descricao) VALUES ("LIT","LITERATURA");
INSERT INTO genero (id, descricao) VALUES ("DID","DIDATICO E TECNICO");
INSERT INTO genero (id, descricao) VALUES ("AAJ","AUTO-AJUDA");
INSERT INTO genero (id, descricao) VALUES ("REL","RELIGIOSO");
INSERT INTO genero (id, descricao) VALUES ("TER","TERROR");
INSERT INTO genero (id, descricao) VALUES ("INF", "INFANTIL");
INSERT INTO genero (id, descricao) VALUES ("DRA","DRAMA");
INSERT INTO genero VALUES ("CIE","DIVULGACAO CIENTIFICA");

/* INSERINDO DADOS NA TABELA obra*/
INSERT INTO obra (id_genero, titulo) VALUES ("LIT", "HELENA");
INSERT INTO obra (id_genero, titulo) VALUES ("LIT", "DOM CASMURRO");
INSERT INTO obra (id_genero, titulo) VALUES ("DID", "TEORIA DO CAOS");
INSERT INTO obra (id_genero, titulo) VALUES ("LIT", "OS TRES MOSQUETEIROS");
INSERT INTO obra (id_genero, titulo) VALUES ("REL", "A HISTORIA PERDIDA E REVELADA");
INSERT INTO obra (id_genero, titulo) VALUES ("LIT", "O CONDE DE MONTECRISTO");
INSERT INTO obra (id_genero, titulo) VALUES ("DID", "SISTEMAS DE BANCO DE DADOS");
INSERT INTO obra (id_genero, titulo) VALUES ("TER", "THE WALKING DEAD");
INSERT INTO obra (id_genero, titulo) VALUES ("INF", "ALICE NO PAIS DAS MARAVILHAS");
INSERT INTO obra (id_genero, titulo) VALUES ("DRA", "CONDE DRACULA");
INSERT INTO obra (id_genero, titulo) VALUES ("CIE", "VIAGEM AO ESPACO");
INSERT INTO obra (id_genero, titulo) VALUES ("REL", "APOCALIPSE");

/* INSERINDO DADOS NA TABELA autor*/
INSERT INTO autor (nome) VALUES("MACHADO DE ASSIS");
INSERT INTO autor (nome) VALUES("ALEXANDRE DUMAS");
INSERT INTO autor (nome) VALUES("CARL SAGAN");
INSERT INTO autor (nome) VALUES("KORTH");/* */
INSERT INTO autor (nome) VALUES("SILBERCHATZ");
INSERT INTO autor (nome) VALUES("S HAWKING");
INSERT INTO autor (nome) VALUES("ARTHUR CLARK");
INSERT INTO autor (nome) VALUES("ROBERT KIRKMAN");
INSERT INTO autor (nome) VALUES("IAN GRAHAM");
INSERT INTO autor (nome) VALUES("JOAO DE PATMOS");
INSERT INTO autor (nome) VALUES("BRAM STOKER");

/* INSERINDO DADOS NA TABELA usuario*/
INSERT INTO usuario (nome, sexo, data_nascimento, endereco) VALUES ("JULIO CESAR", "M", "0010-11-19", "VIA APIA, 331 APT.201, ROMA");
INSERT INTO usuario (nome, sexo, data_nascimento, endereco) VALUES ("CONSTANTINO", "M", "0200-10-18", "VIA ROMANA, 331 APT.201, CONSTANTINOPLA");
INSERT INTO usuario (nome, sexo, data_nascimento, endereco) VALUES ("ANIBAL", "M", "0009-09-17", "VIA MEDITERRANEA, 331 APT.201, CARTAGO");
INSERT INTO usuario (nome, sexo, data_nascimento, endereco) VALUES ("CLEOPATRA", "F", "0011-09-16", "AV NILO, 331 APT.201, TEBAS");
INSERT INTO usuario (nome, sexo, data_nascimento, endereco) VALUES ("JOAQUIM JOSE", "M", "1770-08-15", "AV BEIRA RIO, 331 APT.201, VILA RICA");
INSERT INTO usuario (nome, sexo, data_nascimento, endereco) VALUES ("ISABEL ORLEANS", "F", "1840-07-14", "PRACA PARIS, 331 APT.201, RIO DE JANEIRO");

/* INSERINDO DADOS NA TABELA editora*/
INSERT INTO editora VALUES ("ATICA","ATICA");
INSERT INTO editora VALUES ("MDLTR","MUNDO DAS LETRAS");
INSERT INTO editora VALUES ("LVTEC","LIVRO TECNICO");
INSERT INTO editora VALUES ("ERICA","ERICA");
INSERT INTO editora VALUES ("OXFOR","OXFORD");
INSERT INTO editora VALUES ("AGIR","AGIR");
INSERT INTO editora VALUES ("SICIL","SICILIANO");
INSERT INTO editora VALUES ("GAL","GALERA");

/* INSERINDO DADOS NA TABELA livro*/
INSERT INTO livro (data_aquisicao, edicao, id_obra, id_editora) VALUES ("1985-10-11", 1, 1, "ATICA");
INSERT INTO livro (data_aquisicao, edicao, id_obra, id_editora) VALUES ("1986-11-10", 1, 2, "MDLTR");
INSERT INTO livro (data_aquisicao, edicao, id_obra, id_editora) VALUES ("1987-12-12", 1, 3, "LVTEC");
INSERT INTO livro (data_aquisicao, edicao, id_obra, id_editora) VALUES ("1988-10-13", 1, 4, "ERICA");
INSERT INTO livro (data_aquisicao, edicao, id_obra, id_editora) VALUES ("1989-11-14", 1, 5, "OXFOR");
INSERT INTO livro (data_aquisicao, edicao, id_obra, id_editora) VALUES ("1990-12-15", 1, 6, "AGIR");
INSERT INTO livro (data_aquisicao, edicao, id_obra, id_editora) VALUES ("1991-09-16", 1, 7, "LVTEC");
INSERT INTO livro (data_aquisicao, edicao, id_obra, id_editora) VALUES ("1985-08-17", 1, 8, "GAL");
INSERT INTO livro (data_aquisicao, edicao, id_obra, id_editora) VALUES ("1986-07-18", 1, 9, "AGIR");
INSERT INTO livro (data_aquisicao, edicao, id_obra, id_editora) VALUES ("1987-08-19", 1, 10, "GAL");
INSERT INTO livro (data_aquisicao, edicao, id_obra, id_editora) VALUES ("1988-11-20", 1, 11, "ATICA");
INSERT INTO livro (data_aquisicao, edicao, id_obra, id_editora) VALUES ("1989-09-20", 1, 12, "SICIL");
INSERT INTO livro (data_aquisicao, edicao, id_obra, id_editora) VALUES ("1989-09-20", 1, 12, "SICIL");


/* INSERINDO DADOS NA TABELA autoria*/
INSERT INTO autoria VALUES (1, 1);
INSERT INTO autoria VALUES (2, 1);
INSERT INTO autoria VALUES (3, 4);
INSERT INTO autoria VALUES (4, 3);
INSERT INTO autoria VALUES (5, 2);
INSERT INTO autoria VALUES (6, 6);
INSERT INTO autoria VALUES (7, 5);
INSERT INTO autoria VALUES (8, 8);
INSERT INTO autoria VALUES (9, 9);
INSERT INTO autoria VALUES (10, 11);
INSERT INTO autoria VALUES (11, 10);
INSERT INTO autoria VALUES (12, 11);

/* INSERINDO DADOS NA TABELA movimentacao*/
INSERT INTO movimentacao VALUES (1, 2,  "2012-11-13", "2013-02-01", "2013-02-05", 5.20);
INSERT INTO movimentacao VALUES (2, 12, "2012-12-12", "2013-02-03", "2013-02-05", 5);
INSERT INTO movimentacao VALUES (3, 11,  "2012-11-13", "2013-01-02", "2013-02-05", 5.20);
INSERT INTO movimentacao VALUES (4, 10, "2012-10-14", "2013-01-04", "2013-02-05", 5.20);
INSERT INTO movimentacao VALUES (5, 9, "2012-11-15", "2013-03-05", "2013-02-05", 0);
INSERT INTO movimentacao VALUES (6, 5, "2012-12-16", "2013-03-06", "2013-02-05", 0);
INSERT INTO movimentacao VALUES (6, 4, "2012-10-17", "2013-01-07", "2013-02-05", 5.20);
INSERT INTO movimentacao VALUES (2, 3,  "2012-11-18", "2013-01-08", "2013-02-05", 5.20);
INSERT INTO movimentacao VALUES (1, 1,  "2012-12-19", "2013-02-09", "2013-02-05", 0);
INSERT INTO movimentacao VALUES (2, 6,  "2012-10-10", "2013-02-02", "2013-02-05", 5.20);
INSERT INTO movimentacao VALUES (2, 2,  "2012-10-10", "2013-02-02", null, 0);
INSERT INTO movimentacao VALUES (2, 1,  "2013-05-10", "2013-06-25", null, 0);
INSERT INTO movimentacao VALUES (1, 3,  "2013-05-05", "2013-06-12", null, 0);
INSERT INTO movimentacao VALUES (4, 1,  "2013-05-05", "2013-06-12", null, 0);
INSERT INTO movimentacao VALUES (4, 11, "2013-10-13", "2013-12-02", "2013-12-05", 0);
INSERT INTO movimentacao VALUES (5, 8, "2013-10-17", "2013-11-30", "2013-12-01", 0);
INSERT INTO movimentacao VALUES (3, 8, "2013-11-18", "2013-12-20", "2013-12-22", 0);
INSERT INTO movimentacao VALUES (6, 7, "2013-10-10", "2013-11-20", "2013-12-02", 25.20);
INSERT INTO movimentacao VALUES (2, 2, "2013-11-13", "2013-12-01", "2013-12-03", 5.20);
INSERT INTO movimentacao VALUES (3, 10, "2013-10-14", "2013-11-04", "2013-11-05", 5.20);
INSERT INTO movimentacao VALUES (1, 7, "2013-10-10", "2013-11-20", "2013-11-20", 0);
INSERT INTO movimentacao VALUES (3, 2,  "2013-12-05", "2014-01-12", null, 0);




/* 1. Quais são os livros que temos. Número do livro, Título, Data da aquisição. Ordenar por Título. */
SELECT livro.id, obra.titulo, livro.data_aquisicao 
FROM livro, obra 
WHERE livro.id_obra = obra.id 
ORDER BY obra.titulo;

/* 2. Quais são os livros que temos, por editora. Nome da editora, Título, Data da aquisição. Ordenar por editora. */
SELECT nome_fantasia AS editora, titulo, data_aquisicao 
FROM editora, obra, livro
WHERE livro.id_obra = obra.id AND livro.id_editora = editora.id 
ORDER BY editora;

/* 3. Quais são os livros que temos, por gênero. Gênero (descrição), Título do livro, Data da aquisição. Ordenar por gênero e depois por título. */
SELECT genero.descricao AS genero, obra.titulo, livro.data_aquisicao 
FROM genero, livro, obra 
WHERE livro.id_obra = obra.id AND obra.id_genero = genero.id 
ORDER BY genero.descricao, obra.titulo;

/* 4. Quais são os livros que temos, por gênero e editora. Gênero (descrição), Título do livro, Data da aquisição, Nome da editora. Ordenar por gênero e depois por editora. */
SELECT descricao AS genero, titulo, data_aquisicao, nome_fantasia AS editora 
FROM genero, obra, livro, editora
WHERE livro.id_obra = obra.id
AND livro.id_editora = editora.id
AND obra.id_genero = genero.id 
ORDER BY genero, editora;

/* 5. Quantos livros temos para cada título. Título do livro, Quantidade de livros. */
SELECT obra.titulo, COUNT( livro.id) AS quantidade_de_livros 
FROM obra, livro 
WHERE obra.id = livro.id_obra 
GROUP BY obra.titulo;

/* 6. Quantos livros temos por gênero. Descrição do gênero, Quantidade de livros. Ordenar por gênero. */
SELECT descricao AS genero, COUNT(id_genero) AS quantidade_livros
FROM genero, obra
WHERE genero.id = obra.id_genero
GROUP BY genero;

/* 7. Quantos livros existem por editora? Nome da editora, Quantidade de livros da editora. Ordenar por editora.*/
SELECT editora.nome_fantasia, COUNT(livro.id) AS quantidade_de_livros
FROM editora, livro
WHERE editora.id = livro.id_editora
GROUP BY editora.nome_fantasia;

/* 8 - Quantos livros temos por gênero e por editora. Descrição do gênero, Nome da editora, Quantidade de livros. Ordenar por gênero e por editora. */
SELECT descricao AS genero, nome_fantasia AS editora, COUNT(*) AS quantidade_livros
FROM genero, editora, livro, obra
WHERE genero.id = obra.id_genero AND editora.id = livro.id_editora AND obra.id = livro.id_obra
GROUP BY genero, editora
ORDER BY genero, editora;

/* 9. Quais são os usuários que já pagaram multa. Nome do usuário, Data do empréstimo, Valor pago. Ordenar pelo nome do usuário. */
SELECT usuario.nome, movimentacao.data_emprestimo, SUM(movimentacao.multa) AS valor_total 
FROM usuario, movimentacao 
WHERE usuario.id = movimentacao.id_usuario 
AND movimentacao.multa > 0 
GROUP BY usuario.nome, movimentacao.data_emprestimo 
ORDER BY usuario.nome;

/* 10. Quais são os usuários que já pagaram multa, por livro. Nome do usuário, Título do livro, Data do empréstimo, Valor pago. Ordenar pelo nome do usuário. */
SELECT nome, titulo, data_emprestimo, multa
FROM usuario, obra, livro, movimentacao
WHERE movimentacao.id_usuario = usuario.id AND movimentacao.id_livro = livro.id AND obra.id = livro.id_obra AND multa > 0
ORDER BY nome;

/* 11. Qual o total de multas pago por usuário. Nome do usuário, Quantidade de multas, Valor total das multas pagas. Ordenar por usuário. */
SELECT usuario.nome, COUNT(movimentacao.multa) AS quantidade_multas, SUM(movimentacao.multa) AS valor_total 
FROM usuario, movimentacao 
WHERE usuario.id = movimentacao.id_usuario AND movimentacao.multa > 0 
GROUP BY usuario.nome 
ORDER BY usuario.nome;

/* 12. Quais são os livros mais emprestados? Título do livro, Quantas vezes foi emprestado. Ordenar por “quantas vezes foi emprestado” em ordem decrescente. */
SELECT  titulo, COUNT(titulo) AS quantidade_emprestimo
FROM obra, livro, movimentacao
WHERE livro.id = movimentacao.id_livro AND obra.id = livro.id_obra 
GROUP BY titulo 
ORDER BY quantidade_emprestimo DESC;

/* 13. Quais são os usuários que estão com livro emprestado? (Portanto, ainda não devolveram). Nome do usuário, Título do livro, Data_Emprestimo. Ordenar por Data_Emprestimo e por Nome. */
SELECT usuario.nome, obra.titulo, movimentacao.data_emprestimo
FROM usuario, obra, movimentacao, livro 
WHERE usuario.id = movimentacao.id_usuario AND movimentacao.id_livro = livro.id AND livro.id_obra = obra.id AND movimentacao.data_devolucao IS NULL
ORDER BY movimentacao.data_emprestimo, usuario.nome;

/* 14. Quais são os usuários que ficaram atrasados com a devolução e não pagaram multa? Nome do usuário, Título do livro, Data do empréstimo, Data prevista da devolução, Data da devolução, Número de dias de atraso. */
SELECT nome, titulo, data_emprestimo, data_prevista, data_devolucao, DATEDIFF(data_devolucao, data_prevista) AS dias_atraso 
FROM usuario, obra, movimentacao, livro, editora
WHERE movimentacao.id_usuario = usuario.id 
   AND obra.id = livro.id_obra 
   AND livro.id = movimentacao.id_livro 
   AND editora.id = livro.id_editora 
   AND data_prevista < data_devolucao
   AND multa = 0
ORDER BY data_devolucao;

/* 15. Quais são os usuários que estão atrasados com a devolução? Nome do usuário, Título do livro, Data do empréstimo, Data prevista da devolução, Dias de atraso, Multa prevista até hoje (imagine que a multa é R$ 0,15 por dia de atraso). */
SELECT usuario.nome, obra.titulo, movimentacao.data_emprestimo, movimentacao.data_prevista, 
   CURRENT_DATE AS data_atual, 
   DATEDIFF(CURRENT_DATE , movimentacao.data_prevista) AS dias_atrasados, 
   DATEDIFF(CURRENT_DATE , movimentacao.data_prevista) * 0.15 AS multa_prevista
FROM usuario, movimentacao, livro, obra 
WHERE movimentacao.id_usuario = usuario.id
   AND movimentacao.id_livro = livro.id 
   AND livro.id_obra = obra.id 
   AND movimentacao.data_devolucao IS NULL
   AND CURRENT_DATE > movimentacao.data_prevista 
GROUP BY usuario.nome, obra.titulo;

/* 16. (Repita a consulta anterior para meses). Quais são os usuários atrasados com a devolução? Nome do usuário, Título do livro, Data do empréstimo, Data prevista da devolução, Meses de atraso, Multa prevista até hoje (imagine que a multa é R$ 5,00 por mês de atraso, ou fração). */
SELECT usuario.nome, obra.titulo, movimentacao.data_emprestimo, movimentacao.data_prevista, CURRENT_DATE AS data_atual,
   DATEDIFF(CURRENT_DATE , movimentacao.data_prevista) / 30 AS meses_atrasados,
   DATEDIFF(CURRENT_DATE , movimentacao.data_prevista) / 30 * 5 AS multa
FROM usuario, movimentacao, livro, obra 
WHERE movimentacao.id_usuario = usuario.id
   AND movimentacao.id_livro = livro.id 
   AND livro.id_obra = obra.id 
   AND movimentacao.data_devolucao IS NULL
   AND CURRENT_DATE > movimentacao.data_prevista
GROUP BY usuario.nome, obra.titulo;

/* 17. Quais são os autores que mais escrevem livros? Código do autor, Nome do autor e Quantidade de livros dele. Ver se dá para dizer qual o autor que mais escreveu livros. */
SELECT autor.id, autor.nome, COUNT(autoria.id_obra) AS quantidade_obras
FROM autor, autoria, obra
WHERE autoria.id_autor = autor.id AND autoria.id_obra = obra.id
GROUP BY autor.id;

/* 18. Qual é o nome do usuário que pagou a maior multa? Nome do usuário, Título do livro, Data do empréstimo e Valor da multa paga. */
SELECT nome, titulo, data_emprestimo, multa
FROM usuario, obra, movimentacao, livro, genero
WHERE usuario.id = movimentacao.id_usuario 
   AND obra.id_genero = genero.id
   AND obra.id = livro.id_obra
   AND livro.id = movimentacao.id_livro
   AND movimentacao.multa = (SELECT MAX(multa))
ORDER BY multa DESC
LIMIT 0,1;

/* 19. Qual é o nome do primeiro usuário da biblioteca. Nome do usuário, Título do livro e Data do empréstimo. */
SELECT usuario.nome, obra.titulo, movimentacao.data_emprestimo
FROM usuario, obra, livro, movimentacao
WHERE movimentacao.id_usuario = usuario.id 
	AND movimentacao.id_livro = livro.id 
	AND livro.id_obra = obra.id
	AND movimentacao.data_emprestimo = (SELECT MIN(movimentacao.data_emprestimo))   
ORDER BY data_emprestimo
LIMIT 0,1;

/* 20. Qual é o nome do primeiro usuário que pagou multa. Nome do usuário, Título do livro e Data do empréstimo, Valor da multa. */
SELECT nome, titulo, data_emprestimo,  multa
FROM usuario, obra, movimentacao, livro
WHERE usuario.id = movimentacao.id_usuario 
   AND obra.id = livro.id_obra
   AND livro.id = movimentacao.id_livro
   AND movimentacao.data_prevista < data_devolucao
   AND movimentacao.data_devolucao = (SELECT MIN(data_devolucao))
ORDER BY movimentacao.data_devolucao
LIMIT 0,1;

/* 21. Quais são os usuários que mais pegaram livros, por mês. Mês (ano e mês), Nome do usuário e Quantidade. Ordenar por: Mês (decrescente: ano e mês) e quantidade (decrescente). */
SELECT YEAR(movimentacao.data_emprestimo) AS ano, MONTH(movimentacao.data_emprestimo) AS mes, usuario.nome, COUNT(movimentacao.id_livro) AS quantidade_livros 
FROM usuario, livro, movimentacao
WHERE movimentacao.id_livro = livro.id AND movimentacao.id_usuario = usuario.id
GROUP BY ano, mes, usuario.nome
ORDER BY ano DESC,  mes DESC, quantidade_livros DESC;

/* 22. Quais foram os usuários que pegaram livro no mês xx/xx e ainda não devolveram? Nome do usuário, Título do livro, Data do empréstimo, Data prevista da devolução. */
SELECT nome, titulo, data_emprestimo,  movimentacao.data_prevista
FROM usuario, obra, movimentacao, livro
WHERE usuario.id = movimentacao.id_usuario 
   AND obra.id = livro.id_obra
   AND livro.id = movimentacao.id_livro
   AND YEAR(movimentacao.data_emprestimo)  = 2013
   AND MONTH(movimentacao.data_emprestimo) = 12 
   AND CURRENT_DATE < movimentacao.data_prevista
   AND movimentacao.data_devolucao IS NULL
ORDER BY usuario.nome;
